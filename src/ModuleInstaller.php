<?php

namespace Castor\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ModuleInstaller extends LibraryInstaller {
  public function getInstallPath(PackageInterface $package) {
    $prefix = substr($package->getPrettyName(), 0, 13);
    if ($prefix !== 'ucomm/castor-') {
      throw new \InvalidArgumentException(
        'Unable to install castor module'
      );
    }
    return 'modules/' . substr($package->getPrettyName(), 13);
  }
  public function supports($packageType) {
    return 'castor-module' === $packageType;
  }
}